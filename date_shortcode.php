<?php
/**
 * Plugin Name: date_shortcode
 * Plugin URI: 
 * Description: Returns the current date in a given format.
 * Version: 0.1
 * Author: Deea 
 * Author URI: 
 * License: whatever
 */


function date_func($atts){
	$date = shortcode_atts(array(
       'format' => 'm.d.y',
    ), $atts );
	the_time($date['format']);
}
add_shortcode('shortcodedate','date_func');
?>